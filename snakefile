import glob

# Make a global wildcard so every rule can use the same wildcard.
(SAMPLES,STRANDS) = glob_wildcards("samples/{sample}_{strand}.fastq.gz")
STRANDS = ["R1","R2"]

rule all:
# The so called rule all serves as the main fucntion for snakemake. Rule all calls for the output of the last rule so snakemake has to look for a way to create this file. 
    input:
        expand("Dir_{sample}/{sample}.tree", sample = SAMPLES)

rule bbtools:
# This rule uses the repair function from bbtools. We found that data that has irregular lengths between the R1 and R2 will not function well. So we use the bbtools repair funtion to alleviate this problem. 
    input: 
        raw_fwd = "samples/{sample}_R1.fastq.gz",
        raw_rev = "samples/{sample}_R2.fastq.gz"
    output: 
        fwd = temporary("Dir_{sample}/bbtools/{sample}_R1.fastq.gz"),
        rev = temporary("Dir_{sample}/bbtools/{sample}_R2.fastq.gz")
    conda: 
        "envs/fastp.yaml"
    shell: 
        "repair.sh in1={input.raw_fwd} in2={input.raw_rev} out1={output.fwd} out2={output.rev} repair"

rule fastp_trim:
# This rule uses fastp for adapter trimming and quality assurance for the data. 
    input: 
        raw_fwd = "Dir_{sample}/bbtools/{sample}_R1.fastq.gz",
        raw_rev = "Dir_{sample}/bbtools/{sample}_R2.fastq.gz"
    output: 
        fwd = temporary("Dir_{sample}/fastp/{sample}_R1.fastq.gz"),
        rev = temporary("Dir_{sample}/fastp/{sample}_R2.fastq.gz"),
        html = "Dir_{sample}/fastp/{sample}.html",
        json = "Dir_{sample}/fastp/{sample}.json"
    conda:
        "envs/fastp.yaml"
    shell:
        "fastp -i {input.raw_fwd} -I {input.raw_rev} -o {output.fwd} -O {output.rev} --html {output.html} --json {output.json}"

rule assembly:
# This rule uses the Get Organelle assembler. The output of Get Organelle will always be the same filename, so we  incorperated a mv command to give the assembled file the sample name for continuity 
    input:  
        fwd = "Dir_{sample}/fastp/{sample}_R1.fastq.gz",
        rev = "Dir_{sample}/fastp/{sample}_R2.fastq.gz"
    output: 
        "Dir_{sample}/assembled/{sample}.fasta"
        #maak hier een dir van
    conda:
        "envs/GO.yaml"
    params:
        work_dir = temporary("Dir_{sample}/assembly/")
    threads:
        16
    shell:
        "get_organelle_from_reads.py -1 {input.fwd} -2 {input.rev} -o {params.work_dir} -t {threads} --overwrite -w 65 -F embplant_pt --reduce-reads-for-coverage inf --max-reads inf && mv {params.work_dir}/embplant_pt.K115.complete.graph1.1.path_sequence.fasta {output}"

rule annotate:
# A rule for annotation using PGA, PGA requires two directories. One directory for the reference genomes. And one with the file(s) which will be annotated. 
    input: 
        "Dir_{sample}/assembled/{sample}.fasta"
    output:
        "Dir_{sample}/annotated/{sample}.gb"
    conda:
        "envs/PGA.yaml"
    params:
        assembled = "Dir_{sample}/assembled", 
        annotated = "Dir_{sample}/annotated" 
    shell:   
        "perl scripts/PGA.pl -r refset -t {params.assembled} -o {params.annotated}"

rule gene_extract:
# This rule calls a python script that seaches the annotated file for the barcoding regions used in the multiple sequence alignment.
    input:
        "Dir_{sample}/annotated/{sample}.gb"
    output: 
        "Dir_{sample}/barcoding_regions_{sample}.fa",
        temporary("Dir_{sample}/{sample}.fa")
    script: 
        "scripts/gene_extract.py"

rule file_merger:
# This rule calls the script file_merger, this script reads a file with barcoding region data and the file produced by gene_extract and writes the content of both to a new file.
    input:
        "Dir_{sample}/{sample}.fa"
    output:
        temporary("Dir_{sample}/to_msa_{sample}.fa")
    script:
        "scripts/file_merger.py"

rule msa:
# This rule uses Clustal Omega for the multiple sequence alignment neccesary for making a phylogenetic tree
    input:
        "Dir_{sample}/to_msa_{sample}.fa"
    output:
        "Dir_{sample}/msa_{sample}.fa"
    conda:
        "envs/msatree.yaml"
    shell:
        "clustalo --in={input} --out={output} --outfmt=fa --auto"

rule fasttree:
# This rule uses FastTree to generate a phylogenetic tree 
    input: 
        "Dir_{sample}/msa_{sample}.fa"
    output: 
       "Dir_{sample}/{sample}.tree"
    conda:
        "envs/msatree.yaml"
    shell: 
        "FastTree -nt {input} > {output}"
