import re

def main():
    """ 
    The main function, calls for the file_merger function.

    :return:
    """

    file_merger()

def file_merger():     
    """
    A function to reas the pre-existing file containing gene barcoding regions. And write them and the newly acquired gene barcoding regions to a new file. 

    :return:
    """    

    # Takes the snakemake wildcard to keep the same nameing conventions.
    snake_pre_sample = re.split(r'Dir_|[/]|.fa+', snakemake.input[0])[1]
    snake_sample = str(snake_pre_sample)
    # Declare the files for use.
    files = ['target_barcoding_regions.fa' ,'Dir_'+snake_sample+'/'+snake_sample+'.fa']

    with open('Dir_'+snake_sample+'/to_msa_'+snake_sample+'.fa', 'w') as output_file:
        for name in files:
            with open (name) as input_file:
                output_file.write(input_file.read())
            output_file.write("\n")

if __name__ == "__main__":
    main()
