from Bio import SeqIO
from Bio.SeqFeature import SeqFeature, FeatureLocation
import re


def main():
    """ 
    The main function, calls for the gene_extract function.

    :return:
    """
    gene_extract()

def gene_extract():
    """
    A function to extract DNA barcoding genes and a barcoding region from genbank files. 
    Using BioPython to parse through the genbank files. And for creating new features for the barcoding regions.

    :return:
    """

    # Creating variables. 
    results = {}
    rbcl_seq = ""
    rbcl_selection_start = None
    rbcl_selection_end = None
    spacer_start = None
    spacer_end = None
    starts_for_rbcl = ["AAGTGT","GAGTGT","AGGTGT","TAATGT","AGACAT"]

    # Takes the snakemake wildcard to keep the same nameing conventions.
    snake_pre_sample = re.split(r"Dir_|[/]|.gb+", snakemake.input[0])[1]
    snake_sample = str(snake_pre_sample)

    for rec in SeqIO.parse("Dir_"+snake_sample+"/annotated/"+snake_sample+".gb", "genbank"):
        # Loop over the genbank file.
        if rec.features:
            for feature in rec.features:
                # Loop over each entry in the genbank file.
                if feature.type == "gene":
                    # Search the genes for the gene matK
                    if feature.qualifiers["gene"][0].lower() == "matk":
                        # Fill the results dictionary with genename, nucleotide sequence, start position and end position.
                        results[feature.qualifiers["gene"][0].lower()] = {'seq': str(feature.extract(rec.seq)), 'start': int(feature.location.start.position), 'end': int(feature.location.end.position)}
                    else:   
                        if feature.qualifiers["gene"][0].lower() == "rbcl":
                            rbcl_seq = str(feature.extract(rec.seq))
                            #takes the start of the gene, for later use.
                            rbcl_start = int(feature.location.start.position)
                            # see if one of the starting 6 nucleotides for the barcoding region of rbcl is present.
                            rbcl_start_match = next(x for x in starts_for_rbcl if x in str(rbcl_seq))
                            # get the location of the item found in the list and add the start location of the gene to get the actual location.
                            rbcl_selection_start = str(rbcl_seq).find(rbcl_start_match) + rbcl_start
                            # the rbcl selection should always be 553 nucleotides long so we only want 553 nucleotides.
                            rbcl_selection_end = rbcl_selection_start + 552
                        if feature.qualifiers["gene"][0].lower() == "trnh": 
                            spacer_start = int(feature.location.end.position)
                            
                        if feature.qualifiers["gene"][0].lower() == "psba":
                            spacer_end = int(feature.location.start.position)
            break

    # Create a feature for a fragment the gene rbcL and allocate the start and end of this feature. 
    rbcl_selection = FeatureLocation(int(rbcl_selection_start),int(rbcl_selection_end))
    rbcl_selection_feature = SeqFeature(rbcl_selection)
    # Extract the sequence of the feature rbcl_selection.
    rbcl_selection_seq = rbcl_selection_feature.extract(rec.seq)

    # Create a feature for the psbA-trnH intergenic spacer and allocate the end of the psbA gene and the start of the trnH gene.
    spacer = FeatureLocation(int(spacer_start),int(spacer_end))
    spacer_feature = SeqFeature(spacer)
    # Extract the sequence of the feature spacer.
    spacer_seq = spacer_feature.extract(rec.seq)

    # Add the features to the results dictionary.
    results["rbcl"] = {'seq': str(rbcl_selection_seq), 'start': int(rbcl_selection_start), 'end': int(rbcl_selection_end)}
    results["intergenic spacer trnh - psba"] =  {'seq': str(spacer_seq), 'start': int(spacer_start), 'end': int(spacer_end)}
    
    # Write the results dictionary more readable to a fasta file.
    with open('Dir_'+snake_sample+'/barcoding_regions_'+snake_sample+'.fa','w') as data:
        for k, v in results.items():
            # k for key and v for value.
            data.write('>'+str(k)+' found on position: ' +str(v.get('start'))+'-'+str(v.get('end'))+' in '+snake_sample+'.gb\n'+str(v.get('seq'))+'\n')
    
    # Write the nucleotide sequence with the filename to a fasta file. This is in preparation for the multiple sequence alignment.
    with open('Dir_'+snake_sample+'/'+snake_sample+'.fa','w') as nucleotide_fasta:
        nucleotide_fasta.write('>'+snake_sample+'\n')
        for k, v in results.items():
            # k for key and v for value.
            nucleotide_fasta.write(str(v.get('seq')))
            
if __name__ == "__main__":
    main()
