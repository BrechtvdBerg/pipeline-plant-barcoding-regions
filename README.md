# CHAPI

CHAPI stands for CHloroplast Analysis PIpeline. It is an internship project, an automated pipeline to help identify plants. The pipeline focusses on the chloroplast of the plant. And tries to assemble a circulair chloroplast and uses this for further analysis.

## Description
CHAPI is a pipeline that uses [BBtools](https://jgi.doe.gov/data-and-tools/software-tools/bbtools/) repair function to make sure the paired end data is relatively the same length. [FastP](https://github.com/OpenGene/fastp) is used for preprocessing purposes. The user can gain insight in their data with the generated HTML or JSON file. CHAPI uses [Get Organelle](https://github.com/Kinggerm/GetOrganelle) for the assembly. Get Organelle is a organelle focussed assembler and was chosen because CHAPI is specified for chloroplast analysis. For annotation the Plant Genome Annotator [PGA](https://github.com/quxiaojian/PGA) is implemented. CHAPI will generate a file containing the barcoding regions of the genes: matK, rbcL, psbA and trnH. And uses these barcoding regions to generate a Multiple Sequence Alignement. The software used for the MSA is [Clustal Omega](https://github.com/hybsearch/clustalo). The generated MSA is used for generating a phylogenetic tree with usage of [FastTree](http://www.microbesonline.org/fasttree/).

## Installation

Requirements:
- Python 3.6 or higher
- Conda 
- Cloning of this repository

With these you can run the following lines:
```
conda env create --file requirements.yaml
conda env create
```
You now have a conda environment, when CHAPI is first run it will download and install all additionally required files. So the first run can take exceedingly longer than following runs.

## Usage
For CHAPI to work the user must have a directory from which the pipeline will run. This will be the working directory. This directory must be the one where the snakefile is located. In this directory we need another directory 'samples' this is where the input data goes. Data must be forward and reverse stranded data. And follow the following structure 'samplename_R1_fastq.gz' and 'samplename_R2_fastq.gz' otherwise the pipeline will not recognize the the files. 
Firstly navigate to the working direcory. 

Starting CHAPI:
```
snakemake --conda-frontend conda --cores 16 --use-conda
```
You can always allocate less or more cores. Note that in our tests the pipeline struggles to get good results with less than 8 cores.
CHAPI will take approximately 30-45 minutes with 1 GB of data on 16 cores. 

## Output 
The pipeline will generate a directory, staring with 'Dir_' following the samplename. In this directory you will find the following files and directories:

- [ ] annotated
    - 'samplename'.gb
    - screen.log
    - warning.log
- [ ] assembled
    - 'samplename'.fa
- [ ] fastp
    - 'samplename'.html
    - 'samplename'.json
- 'samplename'.tree
- barcoding_regions_'samplename'.fa
- msa_'samplename'

## Known issues
There are some issues, this section will help you understand what went wrong. Currently there is no solution for alleviating these problems. We are sorry for this in the future alternative options can be explored.

If the pipeline stopped at the 3rd job that means the assembler failed at creating a complete chloroplast.

If the pipeline stopped at the 5th job that means the annotator has not found one of the following genes: rbcL, trnH or psbA
